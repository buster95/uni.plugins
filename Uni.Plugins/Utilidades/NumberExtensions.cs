﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Uni.Plugins.Utilidades
{
    public static class NumberExtensions
    {
        public enum Moneda { NIO = 1, USD = 2 }

        private static string[] unidades = new string[]
        {
            "CERO",
            "UN",
            "DOS",
            "TRES",
            "CUATRO",
            "CINCO",
            "SEIS",
            "SIETE",
            "OCHO",
            "NUEVE"
         };

        private static string[] decenas = new string[]
        {
            "DIE",
            "ONCE",
            "DOCE",
            "TRECE",
            "CATORCE",
            "QUINCE",
            "VEINT",
            "TREINTA",
            "CUARENTA",
            "CINCUENTA",
            "SESENTA",
            "SETENTA",
            "OCHENTA",
            "NOVENTA"
        };

        private static string[] centenas = new string[]
        {
            "",
            "CIEN",
            "DOSCIENTOS",
            "TRESCIENTOS",
            "CUATROCIENTOS",
            "QUINIENTOS",
            "SEISCIENTOS",
            "SETECIENTOS",
            "OCHOCIENTOS",
            "NOVECIENTOS"
        };

        public static string ToWords(this double numero, Moneda tipoMoneda = Moneda.NIO)
        {
            string res, dec = "";
            Int64 entero;
            int decimales;
            double nro;

            try
            {
                nro = Convert.ToDouble(numero);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));
            if (decimales > 0)
            {
                dec = " CON " + decimales.ToString() + "/100 CENTAVOS";
            }

            string tipoMonedaWords = "";
            switch (tipoMoneda)
            {
                case Moneda.NIO:
                    tipoMonedaWords = "CORDOBAS";
                    break;

                case Moneda.USD:
                    tipoMonedaWords = "DOLARES";
                    break;

                default:
                    tipoMonedaWords = "CORDOBAS";
                    break;
            }

            res = toText(Convert.ToDouble(entero)) + " " + tipoMonedaWords + " " + dec;
            return res;
        }

        private static string toText(double value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }
            return Num2Text;
        }

        /// <summary>
        /// Convierte un numero a letras retornada en español
        /// </summary>
        /////////////////////////// <param name="cantidad">cantidad a convertir</param>
        /// <returns>string número convertido a letras</returns>
        public static string ToWords2(this double numeroSended)
        {
            double millon = 1000000;
            double cienmiles = 100000;
            double miles = 10000;
            double mil = 1000;

            string unidad = " PESO ";
            string letras = "";
            string[] decim = numeroSended.ToString().Split('.');
            string decimales = "00/100";
            if (decim.Length > 1)
            {
                decimales = decim[1].PadRight(2, '0') + "/100";
            }
            numeroSended = double.Parse(numeroSended.ToString().Split('.')[0]);

            if (numeroSended <= 0)//si cantidad es igual o menor a cero
            {
                letras = "CERO";
            }

            if (numeroSended > 1)//es un peso
            {
                unidad = " PESOS ";
            }

            //INICIA
            if ((numeroSended / millon) >= 1)//con los millones
            {
                int indice = (int)(numeroSended / millon);//el (int) es un cast, ya que el resultado de dividir cantidad/millon me daria un double asi que le hago el cast a int

                if (indice <= 9)//menos de 10 millones
                {
                    if (indice > 1)
                    {
                        letras = unidades[indice] + " MILLONES";
                    }
                    else
                    {
                        letras = unidades[indice] + " MILLON";
                    }
                }

                if (indice >= 10 && indice <= 15)//igual o mayor de 10 pero menor o igual de 15
                {
                    if (indice == 10)
                    {
                        letras = decenas[indice % 10] + "Z MILLONES";
                    }
                    else
                    {
                        letras = decenas[indice % 10] + " MILLONES";
                    }
                }
                if (indice >= 16 && indice <= 19) // de 16-19 millones
                {
                    letras = decenas[0] + "CI " + unidades[indice % 10] + " MILLONES";
                }
                if (indice >= 20 && indice <= 99)//mas de 20 millones pero menos de 99
                {
                    if (indice > 20 && indice < 30)//por si es 21-29
                    {
                        if ((indice % 10) == 0)
                        {
                            letras = decenas[(indice / 10) + 4] + " E MILLONES";
                        }
                        else
                        {
                            letras = decenas[(indice / 10) + 4] + "I Y " + unidades[indice % 10] + " MILLONES";
                        }
                    }
                    else
                        if ((indice % 10) > 0)//para saber si poner el "Y" a la cadena ejemplo 99 seria noventa Y nueve
                    {
                        letras = decenas[(indice / 10) + 4] + " Y " + unidades[indice % 10] + " MILLONES";
                    }
                    else//en esta caso seria 90
                    {
                        letras = decenas[indice / 10 + 4] + " MILLONES";
                    }

                }

                numeroSended = numeroSended % millon;//termino con los millones ahora quedan los cienmiles

            }

            if ((numeroSended / cienmiles) >= 1)//los cien miles
            {
                int indice = (int)(numeroSended / cienmiles);

                if (indice >= 1)//si tiene cienmiles, si es 999,999 se salta al siguiente if
                {
                    letras += " " + centenas[indice];
                }
                numeroSended = numeroSended % cienmiles;

                if (numeroSended == 0)//solo es cien pongo el mil
                {
                    letras += " MIL";
                }
            }

            if ((numeroSended / miles) >= 1)//los diez miles
            {
                int indice = (int)(numeroSended / miles);
                if (indice > 1)//mayor de  20 mil, aqui solo pongo los miles
                {
                    letras += " " + decenas[indice + 4];
                }

                if ((indice == 2) && (numeroSended % miles) == 0)//si da 20 seria veinte 
                {
                    letras += "E";
                }
                else if (indice == 2)//caso contrario seria veinti
                {
                    letras += "I";
                }
                if (indice > 1)//ya tengo puesto los miles le pongo sus unidades
                {
                    letras += " Y " + unidades[(int)(numeroSended % miles) / 1000] + " MIL";
                }

                if ((numeroSended / mil) >= 10 && (numeroSended / mil) <= 19)//si esta entre 11-15
                {
                    indice = (int)(numeroSended / mil);
                    if (indice == 10)//igual a 10 mil
                    {
                        letras += " " + decenas[0] + "Z MIL";
                    }
                    if (indice > 10 && indice < 16)//entre 11 y 15 mil
                    {
                        letras += " " + decenas[(indice % 10)] + " MIL";
                    }
                    if (indice > 15)//mayor de 15 mil
                    {
                        letras += " " + decenas[(indice / 10) - 1] + "CI" + unidades[indice % 10] + " MIL";
                    }

                }
                numeroSended = numeroSended % mil;

            }
            //else if ((cantidad>0)&&((cantidad % 100000) == 0))//si solo es por ejemplo 900000
            //{
            //    letras += " MIL";
            //}
            //si no llega a diez mil
            //por ejemplo 9999
            //este error lo encontre de pura cagada
            //cuando probaba el codigo
            if ((numeroSended / 1000) >= 1)//los miles
            {
                int indice = (int)(numeroSended / 1000);
                letras += " " + unidades[indice] + " MIL";
                numeroSended = numeroSended % 1000;
            }
            if ((numeroSended / 100) >= 1 || (numeroSended / 10) >= 1 || (numeroSended / 1) >= 1)//y por ultimo la colita, las centenas, decenas o unidades
            {
                int indice = (int)(numeroSended / 100);

                if (indice >= 1)//tiene centenas 100
                {
                    if (indice == 1 && (numeroSended % 100) > 0)//si es 110 seria cien + to
                    {
                        letras += " " + centenas[indice] + "TO";
                    }
                    else
                    {
                        letras += " " + centenas[indice];
                    }
                }
                numeroSended = numeroSended % 100;//las decenas

                if (numeroSended < 10 && numeroSended > 0)//del 1-9 
                {
                    if (numeroSended > 1)//del 2-9
                    {
                        letras += " " + unidades[(int)numeroSended];
                    }
                    else//es uno, le pongo la o al final para que sea "uno"
                    {
                        letras += " " + unidades[(int)numeroSended];
                    }

                }
                if (numeroSended >= 10 && numeroSended <= 15)//del 10-15
                {
                    if (numeroSended == 10)
                    {
                        letras += " " + decenas[(int)(numeroSended % 10)] + "Z";
                    }
                    else
                    {
                        letras += " " + decenas[(int)(numeroSended % 10)];
                    }
                }
                if (numeroSended >= 16)//del 16-99
                {
                    if ((int)(numeroSended / 10) == 1)//del del 16-20
                    {
                        letras += " " + decenas[0] + "CI" + unidades[(int)(numeroSended % 10)];

                    }
                    else if ((int)(numeroSended / 10) > 1) //del 20 en adelante
                    {
                        if ((int)(numeroSended / 10) == 2)
                        {
                            if ((numeroSended % 10) > 0)
                            {
                                letras += " " + decenas[(int)(numeroSended / 10) + 4] + "I" + unidades[(int)(numeroSended % 10)];
                            }
                            else
                            {
                                letras += " " + decenas[(int)(numeroSended / 10) + 4] + "E";
                            }
                        }
                        else
                        {
                            if ((numeroSended % 10) > 0)
                            {
                                letras += " " + decenas[(int)(numeroSended / 10) + 4] + " Y " + unidades[(int)(numeroSended % 10)];
                            }
                            else
                            {
                                letras += " " + decenas[(int)(numeroSended / 10) + 4];
                            }
                        }
                    }
                }
            }
            return letras.Trim() + unidad + decimales;
        }
    }
}
